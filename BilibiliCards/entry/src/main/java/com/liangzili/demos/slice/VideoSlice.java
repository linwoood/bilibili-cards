package com.liangzili.demos.slice;

import com.liangzili.demos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.webengine.WebView;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import ohos.utils.zson.ZSONObject;

import java.util.ArrayList;

public class VideoSlice extends AbilitySlice {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.DEBUG,0x0,AbilitySlice.class.getName());
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_video);

        Text text = (Text) findComponentById(ResourceTable.Id_text);
        text.setText("页面跳转中");

        // 随机图片数组
        int[] resource = {ResourceTable.Media_36e,ResourceTable.Media_36g,ResourceTable.Media_36h,ResourceTable.Media_38p};
        Component component = findComponentById(ResourceTable.Id_image);
        if (component instanceof Image) {
            Image image = (Image) component;
            image.setPixelMap(resource[(int)(Math.random()*3)]);//随机显示一张图片
        }

        String url = "https://m.bilibili.com";

        String param = intent.getStringParam("params");//从intent中获取 跳转事件定义的params字段的值
        if(param !=null){
            ZSONObject data = ZSONObject.stringToZSON(param);
            url = data.getString("url");
        }

        navigateBilibili(url);
    }
    //打开哔哩哔哩APP
    public void navigateBilibili(String url){
        Operation operation = new Intent.OperationBuilder()
                .withAction("android.intent.action.VIEW")  // 因为是打开Android应用
                .withBundleName("tv.danmaku.bili") // 限制包名
                .withUri(Uri.parse(url))
                .build();
        Intent urlIntent = new Intent();
        urlIntent.setOperation(operation);
        startAbility(urlIntent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
