package com.liangzili.demos.widget.slideshow;

import java.util.List;

public class SlideShowEntity {
    public static class Result{

        //标题
        private String title;
        public String getTitle() {
            return title;
        }
        public void setTitle(String title) {
            this.title = title;
        }

        private String img;
        public String getImg() {
            return img;
        }
        public void setImg(String img) {
            this.img = img;
        }


    }
    private List<Result> result;
    public List<Result> getResult() {
        return result;
    }
    public void setResult(List<Result> result) {
        this.result = result;
    }
}
